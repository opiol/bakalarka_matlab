function varargout = gui(varargin)
% GUI MATLAB code for gui.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui

% Last Modified by GUIDE v2.5 06-Jun-2018 22:37:08

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT




% --- Executes just before gui is made visible.
function gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui (see VARARGIN)

% Choose default command line output for gui
handles.output = hObject;
clc

f = fopen('port.ini');
if f < 0
    errordlg('Cannot find port.ini','Configuration Error');
else
handles.SAMPLES = 2048;
handles.s = serial(fscanf(f,'%s'));
handles.s.Terminator = 32;
handles.s.InputBufferSize = handles.SAMPLES*5 ;
handles.s.Timeout = 20;
fopen(handles.s);
fclose(f);

str = ['.m1 7: ']
fprintf(handles.s,str);
str = ['.s1000 2000:']
fprintf(handles.s,str);
str = ['.m3 5: ']
fprintf(handles.s,str);

end

guidata(hObject, handles);
axes(handles.axes1)
handles.refImage = imread('graphics/ref.png');
image(handles.refImage)
axis off
axis image
axes(handles.axes2)
handles.pas1Image = imread('graphics/pas1.png');
image(handles.pas1Image)
axis off
axis image
axes(handles.axes3)
handles.pas2Image = imread('graphics/pas2.png');
image(handles.pas2Image)
axis off
axis image
axes(handles.axes4)
handles.gretzImage = imread('graphics/gretz.png');
image(handles.gretzImage)
axis off
axis image
axes(handles.axes5)
handles.akt1dImage = imread('graphics/akt1d.png');
image(handles.akt1dImage)
axis off
axis image
axes(handles.axes6)
handles.aktHalfImage = imread('graphics/aktHalf.png');
image(handles.aktHalfImage)
axis off
axis image
axes(handles.axes7)
handles.aktFullImage = imread('graphics/aktFull.png');
image(handles.aktFullImage)
axis off
axis image

guidata(hObject,handles)
% UIWAIT makes gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure


varargout{1} = handles.output;


% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)

if handles.checkbox1.Value == 1
    axes(handles.axes1);
    a = zeros(size(handles.refImage));
    a(:,:,2) = a(:,:,2) + 255;
    a = uint8(a);
    image(0.2.*a + 0.8.*handles.refImage);
    axis off
    axis image
else
    axes(handles.axes1);
    image(handles.refImage);
    axis off
    axis image    
end



% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
if handles.checkbox2.Value == 1
    axes(handles.axes2);
    a = zeros(size(handles.pas1Image));
    a(:,:,2) = a(:,:,2) + 255;
    a = uint8(a);
    image(0.2.*a + 0.8.*handles.pas1Image);
    axis off
    axis image
else
    axes(handles.axes2);
    image(handles.pas1Image);
    axis off
    axis image    
end



% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
if handles.checkbox3.Value == 1
    axes(handles.axes3);
    a = zeros(size(handles.pas2Image));
    a(:,:,2) = a(:,:,2) + 255;
    a = uint8(a);
    image(0.2.*a + 0.8.*handles.pas2Image);
    axis off
    axis image
else
    axes(handles.axes3);
    image(handles.pas2Image);
    axis off
    axis image    
end


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

opt = [ get(handles.checkbox1,'Value')
      0 %nepouzity 2 vstup
      get(handles.checkbox2,'Value')
      get(handles.checkbox3,'Value')
      get(handles.checkbox4,'Value')
      get(handles.checkbox5,'Value')
      get(handles.checkbox6,'Value')
      get(handles.checkbox7,'Value')
    ];
    
avg = 0;
top = 4;
bot = -4;
for i = 1:length(opt)
    fprintf(handles.s,'.m2 1:');
    pause(1);
    hold on
    
    if opt(i) == 1
        close(findobj('type','figure','number',i))
        figure(i);
         str = ['.m2 ' i+'0' ': '];
        disp(str);
        fprintf(handles.s,str);                  
            if handles.popupmenu3.Value == 1 
            str = ['.g2048: '];            
            fprintf(handles.s,str);
            data = [0];
            data = fread(handles.s,2048*5);          
            values = strread(char(data));
            end
            if handles.popupmenu3.Value == 2 
            str = ['.g1024: '];
            fprintf(handles.s,str);
            data = [0];
            data = fread(handles.s,1024*5);          
            values = strread(char(data));
                        end
            if handles.popupmenu3.Value == 3 
            str = ['.g512: '];
            fprintf(handles.s,str);
            data = [0];
            data = fread(handles.s,512*5);          
            values = strread(char(data));
                        end
            if handles.popupmenu3.Value == 4 
            str = ['.g256: '];
            fprintf(handles.s,str);
            data = [0];
            data = fread(handles.s,256*5);          
            values = strread(char(data));
            end
            if handles.popupmenu3.Value == 5 
            str = ['.g128: '];
            fprintf(handles.s,str);
            data = [0];
            data = fread(handles.s,128*5);          
            values = strread(char(data));
                        end
            if handles.popupmenu3.Value == 6 
            str = ['.g64: '];
            fprintf(handles.s,str);
            data = [0];
            data = fread(handles.s,64*5);          
            values = strread(char(data));
            end
            

            
            m = 50; %resample factor
            y = values;
            u = linspace(0,length(y),length(y)*m); 
            x = 0:length(y)-1;
            for i_=1:length(u)
                yp(i_) = sum(y.*sinc(x-u(i_)));
            end
            margin = round(length(yp)*0.02);
           
            d = fdesign.lowpass('Fp,Fst,Ap,Ast',200e3,600e3,0.5,120,261*1000*m);             
           Hd = design(d,'FIR');
            yp = filter(Hd,yp);
            yp = yp(margin:end-margin).*0.0019*0.985; % prevod na V a orezani
            t = [1/13060 : 1/13060 : length(yp)/13060]; 
            
            if i == 1
                %avg = mean(yp);
                avg = ( max(yp) - abs(min(yp)))/2 + min(yp); 
                top = max(yp - avg);
                top = top + top*1;
                bot = min(yp - avg);
                bot = bot + bot*1.2;
            end
               yp = yp - avg; 
               
               
            if i == 5
                % 0.53 diff amp gain compensation
                yp = yp*0.53;
                plot(t,yp);       
            else
                yp = -1 * yp;
                plot(t,yp);
                
            end
            ylim([bot top]);
            xlabel('time [ms]');
            ylabel('Voltage [V]');
    end
end


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
%Frequency slider
n = 0; %rad zaokrouhleni 10^n
v = get(hObject,'Value');
value = 10^v;
value = round(value*10^(-n))/(10^(-n));
handles.edit1.String = value;

str = sprintf('.s%d ',value);
str = [str int2str(handles.slider2.Value*1000) ':'];
fprintf(handles.s,str);
disp(str);

% SEND STRING


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
n = 0; %rad zaokrouhleni 10^n
v = get(handles.slider1,'Value');
value = 10^v;
value = round(value*10^(-n))/(10^(-n));
handles.edit2.String = round(get(handles.slider2,'Value'),3);

str = sprintf('.s%d ',value);
str = [str int2str(handles.slider2.Value*1000) ':'];
fprintf(handles.s,str);
disp(str);

% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
value = str2double(get(hObject,'String'));
set(handles.slider1, 'Value', log10(value));

str = sprintf('.s%d ',value);
str = [str int2str(handles.slider2.Value*1000) ':'];
fprintf(handles.s,str);
disp(str);


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double
value = str2double(get(hObject,'String'));
set(handles.slider2, 'Value', (value));


str = ['.s' handles.edit1.String ' ' int2str(handles.slider2.Value*1000)  ':'];
fprintf(handles.s,str);
disp(str);

% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
if handles.checkbox4.Value == 1
    axes(handles.axes4);
    a = zeros(size(handles.gretzImage));
    a(:,:,2) = a(:,:,2) + 255;
    a = uint8(a);
    image(0.2.*a + 0.8.*handles.gretzImage);
    axis off
    axis image
else
    axes(handles.axes4);
    image(handles.gretzImage);
    axis off
    axis image    
end


% --- Executes on button press in checkbox5.
function checkbox5_Callback(hObject, eventdata, handles)
if handles.checkbox5.Value == 1
    axes(handles.axes5);
    a = zeros(size(handles.akt1dImage));
    a(:,:,2) = a(:,:,2) + 255;
    a = uint8(a);
    image(0.2.*a + 0.8.*handles.akt1dImage);
    axis off
    axis image
else
    axes(handles.axes5);
    image(handles.akt1dImage);
    axis off
    axis image    
end



% --- Executes on button press in checkbox6.
function checkbox6_Callback(hObject, eventdata, handles)

if handles.checkbox6.Value == 1
    axes(handles.axes6);
    a = zeros(size(handles.aktHalfImage));
    a(:,:,2) = a(:,:,2) + 255;
    a = uint8(a);
    image(0.2.*a + 0.8.*handles.aktHalfImage);
    axis off
    axis image
else
    axes(handles.axes6);
    image(handles.aktHalfImage);
    axis off
    axis image    
end

% --- Executes on button press in checkbox7.
function checkbox7_Callback(hObject, eventdata, handles)

if handles.checkbox7.Value == 1
    axes(handles.axes7);
    a = zeros(size(handles.aktFullImage));
    a(:,:,2) = a(:,:,2) + 255;
    a = uint8(a);
    image(0.2.*a + 0.8.*handles.aktFullImage);
    axis off
    axis image
else
    axes(handles.axes7);
    image(handles.aktFullImage);
    axis off
    axis image    
end

% --- Executes on mouse press over axes background.
function axes1_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
figure();


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
fclose(handles.s);
% Hint: delete(hObject) closes the figure
delete(hObject);


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.popupmenu1.Value == 1 && handles.popupmenu2.Value == 1
    disp('none');
    str = '.m1 0:';
    fprintf(handles.s,str);
end
if handles.popupmenu1.Value == 1 && handles.popupmenu2.Value == 2
    disp('100n');
    fprintf(handles.s,'.m1 6:');
end
if handles.popupmenu1.Value == 1 && handles.popupmenu2.Value == 3
    disp('10u');
    fprintf(handles.s,'.m1 5:');
end
if handles.popupmenu1.Value == 2 && handles.popupmenu2.Value == 1
    disp('100R');
    fprintf(handles.s,'.m1 8:');
end
if handles.popupmenu1.Value == 2 && handles.popupmenu2.Value == 2
    disp('100R || 100n');
    fprintf(handles.s,'.m1 1:');
end
if handles.popupmenu1.Value == 2 && handles.popupmenu2.Value == 3
    disp('100R || 10u');
    fprintf(handles.s,'.m1 3:');
end
if handles.popupmenu1.Value == 3 && handles.popupmenu2.Value == 1
    disp('10k');
    fprintf(handles.s,'.m1 7:');
end
if handles.popupmenu1.Value == 3 && handles.popupmenu2.Value == 2
    disp('10k || 100n');
    fprintf(handles.s,'.m1 2:');
end
if handles.popupmenu1.Value == 3 && handles.popupmenu2.Value == 3
    disp('10k || 10u');
    fprintf(handles.s,'.m1 4:');
end


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
